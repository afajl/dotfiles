for map in `cd ~/Library/texmf/fonts/map/fontname/; ls *.map`
do
	echo "Adding map $map..."
	updmap --enable Map=$map
	[ $? == 0 ] && echo " ok" || echo " already installed or error"
done
texhash ~/Library/texmf
