% Pauls personal class 

\NeedsTeXFormat		{LaTeX2e}
\ProvidesClass		{paul}
					[2004/04/12 PM class]

\RequirePackage[T1]{fontenc}
\RequirePackage{textcomp}


\PassOptionsToClass{a4paper,oneside,swedish,12pt}{memoir}

% fonts
\newif\iffontbemboavant

% title
\newif\iftitleontop
\newif\iftitlepage

% typeblocks
\newif\ifblockratio
\newif\ifblocknormal

% headers
\newif\ifheadersformal
\newif\ifheaderssimple


% ______________________________________________________________________
% PAGESTYLES

% PM
\newif\ifstylepm
\DeclareOption{pm}{
	\PassOptionsToClass{article}{memoir}
	\stylepmtrue 
	\fontbemboavanttrue
	\titleontoptrue
	\blocknormaltrue
	\headerssimpletrue
}


% UPPSATS
\newif\ifstyleuppsats
\DeclareOption{uppsats}{
	\styleuppsatstrue
	\fontbemboavanttrue
	\titlepagetrue
	\blocknormaltrue
	\headersformaltrue
}



% process options
\ProcessOptions 

% ______________________________________________________________________
% FONTS
\iffontbemboavant
	\RequirePackage{bembo}\normalfont
	\RequirePackage[scaled=.89]{frutiger}
\fi


% ______________________________________________________________________
% COMMON

% Here we load the class
\LoadClass{memoir}


% setup protruding 
\pdfprotrudechars=2
\rpcode\font `-=250
\rpcode\font `'=230
\rpcode\font `.=200
\rpcode\font `,=200
\rpcode\font `;=100
\rpcode\font `:=100
\rpcode\font `!=100
\rpcode\font `?=100
\rpcode\font `)=100

% nice tt font
\renewcommand		{\ttdefault}{pcr}


% localisations
\RequirePackage		[latin1] {inputenc}
\RequirePackage		[swedish] {babel}


% references
\RequirePackage{cite}
\renewcommand\citeleft{(}
\renewcommand\citeright{)}


% XXX to observe
\newcommand{\XXX}{\framebox{\textbf{Fix!}} }


% hyperref
\RequirePackage[hyperindex]{hyperref} % bool: colorlinks
\def\pdfBorderAttrs{/Border [0 0 0]}
\RequirePackage{memhfixc}



% ______________________________________________________________________
% PAGE LAYOUT

% textblock

\setlxvchars{\normalfont}
\ifblockratio

	\addtolength{\lxvchars}{10pt}
	\settypeblocksize		{210mm}         {\lxvchars}
							{*}
	% top bottom
	\setulmargins			{*}             {*}
							{1.2}
	% left right
	\setlrmargins			{*}             {*}
							{1.1}
	\setmarginnotes			{5mm}           {40mm}
							{3mm}
	\setheaderspaces		{*}             {*}
							{1.618}
\fi

\ifblocknormal
	\settypeblocksize		{210mm}         {140mm} {*}
	% top bottom
	\setulmarginsandblock			{30mm}         	{30mm} {1}
	% left right
	\setlrmargins			{*}             {*} {1.1}
	\setmarginnotes			{5mm}           {40mm} {3mm}
	\setheaderspaces		{*}             {*} {1.618}
\fi



\checkandfixthelayout



% ______________________________________________________________________
% TITLE PAGE

% institution \institution{Stockholms Universitet}{Teoretisk Filosofi}{}
\newcommand	{\context}[3]{%
	\gdef\@contextOne{#1}%
	\gdef\@contextTwo{#2}%
	\gdef\@contextThree{#3}}

%XXX fixa

% custom titlepage

\iftitleontop
	\renewcommand{\@maketitle}{%
		\newpage
		\sffamily 
  		\begin{center}
   	     \small
   	     \begin{tabular*}{1.0\textwidth}{@{\extracolsep{\fill}}lr}
		\@contextOne & \@author	\\
		\@contextTwo & \@date 	\\
		\@contextThree &  		\\
  		\end{tabular*}			\\
   	     \vspace{5mm} 
		{\huge\@title} 
		\vspace{8mm} 
		\end{center}
 	 \par}
\fi

\iftitlepage
  \renewcommand{\@maketitle}{%
  %\let\footnote\@mem@titlefootkill
  \thispagestyle{empty}%
  \newpage
  \null
  \vfill
  \begin{center}
  \sffamily
  \begin{tabular}{l}
 	\@contextOne\\ \@contextTwo\\ \@contextThree 	\\ 
	\vspace{12mm}				\\
	\huge \@title 				\\ 
	\vspace{14mm}				\\
  	\hfill \@date 				\\
  	\hfill \@author				\\
	\vspace{16mm}				\\
  \end{tabular}
  \end{center}
  \vfill
  \par
  \newpage
  }
\fi


%%% TYPOGRAPHY

%% Misc
\renewcommand{\descriptionlabel}[1]{\hspace\labelsep\textit{#1}}
\tightlists

\maxtocdepth{subsection}

\ifheaderssimple
	% number subsubsection
	\setsecnumdepth			{part}
	% set up headers
	\renewcommand			{\chapnumfont} 		{\normalfont\Large\sffamily}
	\renewcommand			{\chaptitlefont}	{\chapnumfont}
	\setsecheadstyle		{\large\sffamily\raggedright}
	\setsubsecheadstyle		{\normalsize\sffamily\raggedright}
	\setsubsubsecheadstyle	{\normalsize\textit\sffamily\raggedright}
	\setparaheadstyle		{\sffamily}

	\renewcommand{\printchaptername}{}
	\renewcommand{\chapternamenum}{}
	\renewcommand{\afterchapternum}{\space}
\fi


\ifheadersformal
	% number subsubsection
	\setsecnumdepth			{part}
	% set up headers
	\renewcommand			{\chapnumfont} 		{\normalfont\Large\sffamily}
	\renewcommand			{\chaptitlefont}	{\chapnumfont}
	\setsecheadstyle		{\large\sffamily\raggedright}
	\setsubsecheadstyle		{\normalsize\sffamily\raggedright}
	\setsubsubsecheadstyle	{\normalsize\textit\sffamily\raggedright}
	\setparaheadstyle		{\sffamily}

	\renewcommand{\printchaptername}{}
	\renewcommand{\chapternamenum}{}
	\renewcommand{\afterchapternum}{\space}
\fi




% no pageheader
\pagestyle{plain}


%%% REFERENCE FOR LOCAL USE

% rowspace 
%\usepackage{setspace}
%\doublespacing % \onehalfspacing | \setstretch{1.6}


% use roman in enumerations
%\renewcommand{\labelenumi}{\roman{enumi}}


% graphicx
%\RequirePackage[pdftex]{graphicx}
%\graphicspath{{draw/}{pic/}}


% fancyvrb - good looking code
%\usepackage{fancyvrb}
%\fvset{frame=single,commandchars=\\\{\},fontfamily=courier}




