% Pauls personal class
%
%
%

\NeedsTeXFormat		{LaTeX2e}
\ProvidesClass		{uppsats}
					[2004/04/12 UPPSATS class]

%loadfont
\RequirePackage[T1]{fontenc}
\RequirePackage{textcomp}
\RequirePackage{bembo}\normalfont
\RequirePackage[scaled=.89]{avenir}

% Pass options to the parent class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{memoir}}


\ProcessOptions 
\LoadClass[a4paper,article,oneside,swedish,12pt]{memoir}

\typeout{**}
\typeout{**	UPPSATS class}
\typeout{**}



%%% COMMON
\InputIfFileExists{common.sty}



%%% PAGE LAYOUT
% textblock
\setlxvchars{\normalfont}

\settypeblocksize		{210mm}         {140mm} {*}
% top bottom
\setulmarginsandblock	{30mm}         	{30mm} {1}
% left right
\setlrmargins			{*}             {*} {1.1}
\setmarginnotes			{5mm}           {40mm} {3mm}
\setheaderspaces		{*}             {*} {1.618}

\checkandfixthelayout



%%% TITLE PAGE

% institution \institution{Stockholms Universitet}{Teoretisk Filosofi}{}
\newcommand	{\context}[3]{%
	\gdef\@contextOne{#1}%
	\gdef\@contextTwo{#2}%
	\gdef\@contextThree{#3}}


% custom titlepage
\renewcommand{\@maketitle}{%
  %\let\footnote\@mem@titlefootkill
  \thispagestyle{empty}%
  \newpage
  \null
  \vfill
  \begin{center}
  \sffamily
  \begin{tabular}{l}
 	\@contextOne\\ \@contextTwo\\ \@contextThree 	\\ 
	\vspace{12mm}				\\
	\huge \@title 				\\ 
	\vspace{14mm}				\\
  	\hfill \@date 				\\
  	\hfill \@author				\\
	\vspace{16mm}				\\
  \end{tabular}
  \end{center}
  \vfill
  \par
  \newpage
  }


%%% TYPOGRAPHY
% number subsubsection
\renewcommand{\descriptionlabel}[1]{\hspace\labelsep\textit{#1}}
\tightlists

% number subsubsection
\setsecnumdepth			{subsection}
\maxtocdepth			{subsection}

% set up headers
\renewcommand			{\chapnumfont}{\normalfont\Large\sffamily}
\renewcommand			{\chaptitlefont} {\chapnumfont}

\setsecheadstyle		{\large\sffamily\raggedright}
\setsubsecheadstyle		{\normalsize\sffamily\raggedright}
\setsubsubsecheadstyle	{\normalsize\textit\sffamily\raggedright}
\setparaheadstyle		{\sffamily}


% make chapters look like article 
%\renewcommand{\printchaptername}{}
%\renewcommand{\chapternamenum}{}
%\renewcommand{\afterchapternum}{\space}

% no pageheader
\pagestyle{plain}


%%% REFERENCE FOR LOCAL USE

% rowspace 
%\usepackage{setspace}
%\doublespacing % \onehalfspacing | \setstretch{1.6}


% use roman in enumerations
%\renewcommand{\labelenumi}{\roman{enumi}}




% fancyvrb - good looking code
%\usepackage{fancyvrb}
%\fvset{frame=single,commandchars=\\\{\},fontfamily=courier}


