% Pauls personal class
%
%
%

\NeedsTeXFormat		{LaTeX2e}
\ProvidesClass		{cv}
					[2004/04/12 PM class]

%loadfont
\RequirePackage[T1]{fontenc}
\RequirePackage{textcomp}
\RequirePackage{bembo}\normalfont
\RequirePackage[scaled=.89]{avenir}

% Pass options to the parent class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{memoir}}

\ProcessOptions 
\LoadClass[a4paper,article,oneside,swedish,12pt]{memoir}

\typeout{**}
\typeout{**	CV class}
\typeout{**}


%%% COMMON
\InputIfFileExists{common.sty}


%%% PAGE LAYOUT

% textblock
\setlxvchars{\normalfont}

\settypeblocksize		{210mm}         {140mm} {*}
% top bottom
\setulmarginsandblock	{30mm}         	{30mm} {1}
% left right
\setlrmargins			{*}             {*} {1.1}
\setmarginnotes			{5mm}           {40mm} {3mm}
\setheaderspaces		{*}             {*} {1.618}

\checkandfixthelayout




%%% TITLE PAGE



% institution \institution{Stockholms Universitet}{Teoretisk Filosofi}{}
\newcommand	{\att}[3]{%
	\gdef\@attOne{#1}%
	\gdef\@attTwo{#2}%
	\gdef\@attThree{#3}}
 
\newcommand	{\from}[4]{%
	\gdef\@fromOne{#1}%
	\gdef\@fromTwo{#2}%
	\gdef\@fromThree{#3}%
  \gdef\@fromFour{#4}}

%XXX fixa

% custom titlepage
\renewcommand{\@maketitle}{%
	\newpage
	\sffamily 
  	\begin{center}
        \small
        \begin{tabular*}{1.0\textwidth}{@{\extracolsep{\fill}}lr}
  Att: \@attOne & \@fromOne	\\
	\@attTwo & \@fromTwo 	\\
	\@attThree & \@fromThree 		\\
	 & \@fromFour  	\\
  	\end{tabular*}			\\
        \vspace{5mm} 
	{\huge\@title} 
	\vspace{8mm} 
	\end{center}
  \par}


%%% TYPOGRAPHY

%% Misc
\renewcommand{\descriptionlabel}[1]{\hspace\labelsep\sffamily{#1}}
\tightlists


% number subsubsection
\setsecnumdepth			{part}
\maxtocdepth			{subsection}

% set up headers
\renewcommand			{\chapnumfont}{\normalfont\Large\sffamily}
\renewcommand			{\chaptitlefont} {\chapnumfont}

\setsecheadstyle		{\large\sffamily\raggedright}
\setsubsecheadstyle		{\normalsize\sffamily\raggedright}
\setsubsubsecheadstyle	{\normalsize\textit\sffamily\raggedright}
\setparaheadstyle		{\sffamily}

% make chapters look like article 
%\renewcommand{\printchaptername}{}
%\renewcommand{\chapternamenum}{}
%\renewcommand{\afterchapternum}{\space}

% no pageheader
\pagestyle{plain}


%%% REFERENCE FOR LOCAL USE

% rowspace 
%\usepackage{setspace}
%\doublespacing % \onehalfspacing | \setstretch{1.6}


% use roman in enumerations
%\renewcommand{\labelenumi}{\roman{enumi}}


% graphicx
%\RequirePackage[pdftex]{graphicx}
%\graphicspath{{draw/}{pic/}}


% fancyvrb - good looking code
%\usepackage{fancyvrb}
%\fvset{frame=single,commandchars=\\\{\},fontfamily=courier}




