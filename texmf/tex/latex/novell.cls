% Pauls personal class
%
%
%

\NeedsTeXFormat		{LaTeX2e}
\ProvidesClass		{novell}
					[2004/04/12 NOVELL class]

% Pass options to the parent class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{memoir}}


\ProcessOptions 
\LoadClass[a4paper,oneside,swedish]{memoir}

\typeout{**}
\typeout{**	NOVELL class}
\typeout{**}



%%% COMMON

% select typeface 
\usepackage			{ae,aecompl}
\renewcommand		{\ttdefault}
					{pcr}


% localisations
\usepackage         [utf8]
                    {inputenc}
%\RequirePackage		[latin1]
%					{inputenc}

\RequirePackage		[swedish]
					{babel}


% XXX to observe
\newcommand{\XXX}{\framebox{\textbf{Fix!}} }


% hyperref
\RequirePackage[hyperindex]{hyperref} % bool: colorlinks
\def\pdfBorderAttrs{/Border [0 0 0]}
\RequirePackage{memhfixc}




%%% PAGE LAYOUT

% textblock
\settypeblocksize		{*}
						{\lxvchars}
						{1.618}

% top bottom
\setulmargins			{*}
						{*}
						{1.2}

% left right
\setlrmargins			{*}
						{*}
						{1.1}

\setheaderspaces		{*}
						{*}
						{1.618}
\checkandfixthelayout




%%% TITLE PAGE

% custom titlepage
\renewcommand{\@maketitle}{%
  \let\footnote\@mem@titlefootkill
  \newpage
  \null
  \vfill
  \begin{center}
	\huge \@title \par
	\vspace{14mm}
  	\large \@author \par
	\vspace{16mm}
  \end{center}
  \vfill
  \par}




%%% TYPOGRAPHY

% no numbers except part
\setsecnumdepth			{part}

% center chapters
\renewcommand{\printchaptertitle}[1]{%
	\begin{center}
	\normalfont\LARGE#1\par\nobreak
	\end{center}}

% no pageheader
\pagestyle{plain}


%%% REFERENCE FOR LOCAL USE

% rowspace 
%\usepackage{setspace}
%\doublespacing % \onehalfspacing | \setstretch{1.6}


% use roman in enumerations
%\renewcommand{\labelenumi}{\roman{enumi}}


% graphicx
%\RequirePackage[pdftex]{graphicx}
%\graphicspath{{draw/}{pic/}}


% fancyvrb - good looking code
%\usepackage{fancyvrb}
%\fvset{frame=single,commandchars=\\\{\},fontfamily=courier}


