% Pauls personal class
%
%
%

\NeedsTeXFormat		{LaTeX2e}
\ProvidesClass		{plugg}
					[2004/04/12 PLUGG class]


%loadfont
%loadfont
\RequirePackage[T1]{fontenc}
\RequirePackage{textcomp}
\RequirePackage{bembo}\normalfont
\RequirePackage[scaled=.89]{avenir}


% Pass options to the parent class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{memoir}}


\ProcessOptions 
\LoadClass[a4paper,article,oneside,swedish,12pt]{memoir}

\typeout{**}
\typeout{**	PLUGG class}
\typeout{**}



%%% COMMON
\InputIfFileExists{common.sty}


%%% PAGE LAYOUT

% textblock
\setlxvchars{\normalfont}
\addtolength{\lxvchars}{10pt}
\settypeblocksize		{210mm} {\lxvchars} {*}
% top bottom
\setulmargins			{*} {*} {1.2}
% left right
\setlrmargins			{*} {*} {1.1}
\setmarginnotes			{5mm} {40mm} {3mm}


\setheaderspaces		{*} {*} {1.618}
\checkandfixthelayout




%%% TITLE PAGE
% institution \institution{Stockholms Universitet}{Teoretisk Filosofi}{}
\newcommand	{\context}[3]{%
	\gdef\@contextOne{#1}%
	\gdef\@contextTwo{#2}%
	\gdef\@contextThree{#3}}

%XXX fixa

\usepackage{multirow}
% custom titlepage
\renewcommand{\@maketitle}{%
	\newpage
	\sffamily\scriptsize
	\noindent
    \begin{tabularx}{\textwidth}[t]{@{\extracolsep{\fill}}Xr}
	 \multirow{3}{*}{\raggedright\arraybackslash\Large\@title} 
	   & \@contextOne\\
	   & \@contextTwo\\
	   & \@contextThree\\
	   & \@author\\
	   & \@date\\
	\end{tabularx}
	\par}



%%% TYPOGRAPHY

\renewcommand{\descriptionlabel}[1]{\hspace\labelsep\textit{#1}}
\tightlists

%% Misc
%\renewcommand*{\descriptionlabel}[1]{\hspace\labelsep\textsc{#1}}

%% Headers
% number subsection
\setsecnumdepth			{section}

% set up headers
\renewcommand			{\chapnumfont}
						{\normalfont\large\sffamily}

\renewcommand			{\chaptitlefont}
						{\chapnumfont}

\setsecheadstyle		{\normalsize\sffamily\raggedright}

\setsubsecheadstyle		{\normalsize\sffamily\raggedright}

\setsubsubsecheadstyle	{\normalsize\itshape\sffamily\raggedright}

\setparaheadstyle		{\sffamily}

% make chapters look like article 
%\renewcommand{\printchaptername}{}
%\renewcommand{\chapternamenum}{}
%\renewcommand{\afterchapternum}{\space}

% no pageheader
\pagestyle{plain}


%%% REFERENCE FOR LOCAL USE

% rowspace 
%\usepackage{setspace}
%\doublespacing % \onehalfspacing | \setstretch{1.6}


% use roman in enumerations
%\renewcommand{\labelenumi}{\roman{enumi}}


% graphicx
%\RequirePackage[pdftex]{graphicx}
%\graphicspath{{draw/}{pic/}}


% fancyvrb - good looking code
%\usepackage{fancyvrb}
%\fvset{frame=single,commandchars=\\\{\},fontfamily=courier}


