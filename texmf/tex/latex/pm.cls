% Pauls personal class
%
%
%

\NeedsTeXFormat		{LaTeX2e}
\ProvidesClass		{pm}
					[2004/04/12 PM class]

%loadfont
\RequirePackage[utf8]{fontenc}
\usepackage         [utf8]
                    {inputenc}
\RequirePackage{textcomp}
\RequirePackage{bembo}\normalfont
\RequirePackage[scaled=.89]{avenir}

% Pass options to the parent class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{memoir}}

\ProcessOptions 
\LoadClass[a4paper,article,oneside,swedish,12pt]{memoir}

\typeout{**}
\typeout{**	PM class}
\typeout{**}


%%% COMMON
\InputIfFileExists{common.sty}


%%% PAGE LAYOUT

% textblock
\setlxvchars{\normalfont}

\settypeblocksize		{210mm}         {140mm} {*}
% top bottom
\setulmarginsandblock	{30mm}         	{30mm} {1}
% left right
\setlrmargins			{*}             {*} {1.1}
\setmarginnotes			{5mm}           {40mm} {3mm}
\setheaderspaces		{*}             {*} {1.618}

\checkandfixthelayout




%%% TITLE PAGE



% institution \institution{Stockholms Universitet}{Teoretisk Filosofi}{}
\newcommand	{\context}[3]{%
	\gdef\@contextOne{#1}%
	\gdef\@contextTwo{#2}%
	\gdef\@contextThree{#3}}

%XXX fixa

% custom titlepage
\renewcommand{\@maketitle}{%
	\newpage
	\sffamily 
  	\begin{center}
        \small
        \begin{tabular*}{1.0\textwidth}{@{\extracolsep{\fill}}lr}
	\@contextOne & \@author	\\
	\@contextTwo & \@date 	\\
	\@contextThree &  		\\
  	\end{tabular*}			\\
        \vspace{5mm} 
	{\huge\@title} 
	\vspace{8mm} 
	\end{center}
  \par}


%%% TYPOGRAPHY

%% Misc
\renewcommand{\descriptionlabel}[1]{\hspace\labelsep\textit{#1}}
\tightlists


% number subsubsection
\setsecnumdepth			{section}
\maxtocdepth			{subsection}

% set up headers
\renewcommand			{\chapnumfont}{\normalfont\Large\sffamily}
\renewcommand			{\chaptitlefont} {\chapnumfont}

\setsecheadstyle		{\large\sffamily\raggedright}
\setsubsecheadstyle		{\normalsize\sffamily\raggedright\textbf}
\setsubsubsecheadstyle	{\normalsize\sffamily\textit\raggedright}
\setparaheadstyle		{\sffamily}

% make chapters look like article 
%\renewcommand{\printchaptername}{}
%\renewcommand{\chapternamenum}{}
%\renewcommand{\afterchapternum}{\space}

% no pageheader
\pagestyle{plain}


%%% REFERENCE FOR LOCAL USE

% rowspace 
%\usepackage{setspace}
%\doublespacing % \onehalfspacing | \setstretch{1.6}


% use roman in enumerations
%\renewcommand{\labelenumi}{\roman{enumi}}


% graphicx
\RequirePackage[pdftex]{graphicx}
\graphicspath{{draw/}{pic/}}


% fancyvrb - good looking code
%\usepackage{fancyvrb}
%\fvset{frame=single,commandchars=\\\{\},fontfamily=courier}




