% Pauls personal class
%
%
%

\NeedsTeXFormat		{LaTeX2e}
\ProvidesClass		{brev}
					[2004/08/07 BREV class]

% Pass options to the parent class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{letter}}


\ProcessOptions 
\LoadClass[a4paper,swedish]{letter}

\typeout{**}
\typeout{**	BREV class}
\typeout{**}



%%% COMMON

% select typeface 
\usepackage			{ae,aecompl}
\renewcommand		{\ttdefault}
					{pcr}


% localisations
\RequirePackage		[latin1]
					{inputenc}

\RequirePackage		[swedish]
					{babel}


% XXX to observe
\newcommand{\XXX}{\framebox{\textbf{Fix!}} }


% hyperref
\RequirePackage[hyperindex]{hyperref} % bool: colorlinks
\def\pdfBorderAttrs{/Border [0 0 0]}



