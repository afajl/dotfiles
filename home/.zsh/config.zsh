export EDITOR=vim
export TERM=xterm-256color
#export PROJ_DIRS_GLOB=(~/src/{github.com,bitbucket.org,gitlab.com,git.svt.se}/*)
#export PROJ_DIRS="${PROJ_DIRS_GLOB}"

# use jk to exit insert mode
bindkey -M viins 'jk' vi-cmd-mode
