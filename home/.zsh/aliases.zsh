if command -v nvim -> /dev/null; then
    alias vim=nvim
fi
alias ff='fd'
alias gg='rg'
alias youtube-dl-audio='youtube-dl -f "bestaudio[ext=m4a]" --embed-thumbnail --add-metadata '
