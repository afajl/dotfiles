if [[ "$(uname -s)" == "Darwin" ]]; then
    mode=$(defaults read -g AppleInterfaceStyle 2>/dev/null)

    echo -ne "\033]50;SetProfile=$mode\a"
    export COLOR_MODE=$mode
fi
