autoload -U colors && colors

if (( $+commands[git] ))
then
    git="$commands[git]"
else
    git="/usr/bin/git"
fi


_git_dirty() {
    st=$($git status 2>/dev/null | tail -n 1)
    if [[ $st == "" ]]
    then
        echo ""
    else
        if [[ "$st" =~ ^nothing ]]
        then
            echo " on %{$fg_bold[blue]%}$(_git_prompt_info)%{$reset_color%}"
        else
            echo " on %{$fg_bold[red]%}$(_git_prompt_info)%{$reset_color%}"
        fi
    fi
}

_git_prompt_info () {
    ref=$($git symbolic-ref HEAD 2>/dev/null) || return
    echo "${ref#refs/heads/}"
}

_unpushed () {
    $git cherry -v @{upstream} 2>/dev/null
}

_need_push () {
    if [[ $(_unpushed) == "" ]]
    then
        echo " "
    else
        echo " with %{$fg_bold[red]%}unpushed%{$reset_color%}"
    fi
}

function _hg_prompt_info {
    hg prompt --angle-brackets "\
< on %{$fg[magenta]%}<branch></%{$fg[blue]%}<bookmark>%{$reset_color%}>%{$reset_color%}>\
< at %{$fg[yellow]%}<tags|%{$reset_color%}, %{$fg[yellow]%}>%{$reset_color%}>\
%{$fg[green]%}<status|modified|unknown><update>%{$reset_color%}" 2>/dev/null
}

_virtualenv_info () {
    if [[ -n $VIRTUAL_ENV ]]
    then
        echo " using %{$fg_bold[red]%}$(basename $VIRTUAL_ENV)%{$reset_color%}"
    fi
}

_host_name() {
    echo "%{$fg_bold[blue]%}%m%{$reset_color%}"
}

_directory_name() {
    echo "%{$fg_bold[cyan]%}%~%{$reset_color%}"
}

_time() {
    print -P "%F{242}%T%f"
}

vim_ins_mode="%{$fg[grey]%}›%{$reset_color%}"
vim_cmd_mode="%{$fg[green]%}❭%{$reset_color%}"
vim_mode=$vim_ins_mode

function zle-keymap-select {
    vim_mode="${${KEYMAP/vicmd/${vim_cmd_mode}}/(main|viins)/${vim_ins_mode}}"
    zle reset-prompt
}
zle -N zle-keymap-select

function zle-line-finish {
    vim_mode=$vim_ins_mode
}
zle -N zle-line-finish

PROMPT=$'\n%n@$(_host_name):$(_directory_name)$(_virtualenv_info)$(_hg_prompt_info)$(_git_dirty)$(_need_push)$(_time) \n${vim_mode} '
