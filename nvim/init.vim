" vim: set sw=4 ts=4 sts=4 et tw=78 foldlevel=0 foldmethod=marker:
" ###########################
" ### PAULVIMRC 4-06-1999 ###
" ###########################
"
" za - toggle fold under cursor
" zA - toggle all folds under cursor recursivly
" zR - open all folds
" zM - close all folds

" General {{{
set mouse=a                     " Make mouse work (in tmux)
set mousehide                   " Hide mouse when typing
scriptencoding utf-8
set encoding=utf-8
set fileencoding=utf-8

set clipboard=unnamed

set shortmess=AaoIc             " Ignore opened files and no message
set history=1000                " Alot of history
set hidden                      " Allow buffer switching without saving
set cmdheight=2                 " Better display for messages
set signcolumn=yes              " Always show signcolumns

set spelllang=sv
"set spellfile=g:vimdir/words.utf8.add

set tags=./tags;/
set printoptions=duplex:off,number:y,paper:a4,syntax:y,wrap:y

set path+=~/text
set suffixesadd+=.md

set autowrite

set grepprg=rg\ --color=never

nnoremap <Space> <Nop>
let mapleader = " "                     " Instead of \
let maplocalleader = "  "


" Backup {{{
set backup
set writebackup
set backupdir=~/.nvim_backup
silent !mkdir ~/.nvim_backup > /dev/null 2>&1

" fix editing crontab on mac os x
set     backupskip=/tmp/*,/private/tmp/*"

if has("persistent_undo")
	set undofile
	set undolevels=1000
	set undoreload=10000
endif
" }}}
" }}}

" Plugins {{{
call plug#begin('~/.local/share/nvim/plugged')

" VIM enhancements
Plug 'rakr/vim-one'                   " Colorscheme
Plug 'haya14busa/incsearch.vim'     " auto toggle hlsearch
Plug 'farmergreg/vim-lastplace'     " open files on last place
Plug 'airblade/vim-rooter'          " change dir to current buffer
Plug 'tpope/vim-eunuch'               " Unix commands like Rename
Plug 'tpope/vim-commentary'         " commenter, gcc - comment line
                                    "            gc - comment motion or visual
Plug 'editorconfig/editorconfig-vim' " brew install editorconfig
                                     " use :EditorConfigReload to reload
Plug 'junegunn/fzf', { 'do': './install --bin' }
Plug 'junegunn/fzf.vim'

Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Languages
Plug 'rust-lang/rust.vim'
call plug#end()

" }}}

" Plugin Settings {{{

" FZF {{{
nnoremap <Leader>fb :Buffers<cr>
nnoremap <Leader>fh :History:<cr>
nnoremap <Leader>ff :Files<cr>
nnoremap <Leader>fl :Lines<cr>
nnoremap <Leader>fg :Ag<cr>
nnoremap <Leader>fH :Helptags<cr>
nnoremap <Leader>fM :Maps<cr>

nnoremap <silent> <Leader>fp :call fzf#run(fzf#wrap({
\   'source': $FZF_DEFAULT_COMMAND . " --no-messages " . $PROJ_DIRS,
\   'options': '--delimiter / --with-nth 5..',
\   'sink': 'e'
\ }))<cr>

" hledger
inoremap <expr> <c-l><c-a> fzf#vim#complete(fzf#wrap({
    \ 'source': 'hledger accounts'}))

inoremap <expr> <c-l><c-n> fzf#vim#complete(fzf#wrap({
    \ 'source': 'hledger print -Ocsv \|cut -d, -f 6 \| sed s/\"//g \| uniq'}))


let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

" }}}

" UltiSnips {{{
let g:UltiSnipsSnippetsDir="~/.config/nvim/ultisnips"
let g:UltiSnipsJumpForwardTrigger = '<c-l>'
let g:UltiSnipsJumpBackwardTrigger = '<c-h>'
" }}}

" Coc {{{

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction
" Use <c-.> to trigger completion.
inoremap <silent><expr> <c-.> coc#refresh()
" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
" inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Or use `complete_info` if your vim support it, like:
inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

" 'Smart' nevigation
nmap <silent> E <Plug>(coc-diagnostic-prev)
nmap <silent> W <Plug>(coc-diagnostic-next)
nmap <silent> cd <Plug>(coc-definition)
nmap <silent> cy <Plug>(coc-type-definition)
nmap <silent> ci <Plug>(coc-implementation)
nmap <silent> cu <Plug>(coc-references)
nmap <silent> cr <Plug>(coc-rename)
" Use K to shoc documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" }}}

" Rust {{{
let g:rustfmt_autosave = 1
let g:rustfmt_emit_files = 1
let g:rustfmt_fail_silently = 0
" }}}

" }}}

" Formatting {{{

set nowrap                      " Do not wrap long lines
set autoindent                  " Indent at the same level of the previous line
set shiftwidth=4                " Use indents of 4 spaces
set expandtab                   " Tabs are spaces, not tabs
set tabstop=4                   " An indentation every four columns
set softtabstop=4               " Let backspace delete indent
set nojoinspaces                " Prevents inserting two spaces after punctuation on a join (J)
set splitright                  " Puts new vsplit windows to the right of the current
set splitbelow                  " Puts new split windows to the bottom of the current
set cinwords=if,else,elsif,while,class,sub,until,foreach,\\begin,def,class
set iskeyword-=.                " '.' is an end of word designator
set iskeyword-=#                " '#' is an end of word designator
set iskeyword-=-                " '-' is an end of word designator



set formatoptions+=r            " auto insert comment on enter
set formatoptions+=n            " recognize numbered lists
set formatoptions+=2            " use second line in par for indent
set formatoptions+=j            " remove comment on join lines
" }}}

" UI {{{

syntax on
set showmode                    " Display the current mode
set showcmd                     " Show partial commands in status line and

" Only use cursorline in current window
"augroup CursorLine
"	au!
"	au VimEnter,WinEnter,BufWinEnter,InsertLeave * setlocal cursorline
"	au WinLeave,InsertEnter * setlocal nocursorline
"augroup END

set fillchars="vert:|"

set backspace=indent,eol,start  " Backspace for dummies
set linespace=0                 " No extra spaces between rows
set showmatch                   " Show matching brackets/parenthesis
set incsearch                   " Find as you type search
set nohlsearch                  " Don't hightlight searches
set winminheight=0              " Windows can be 0 line high
set ignorecase                  " Case insensitive search
set smartcase                   " Case sensitive when uc present
set completeopt=longest,menuone
"set relativenumber              " Show relative line numbers
set cmdheight=2                 " Better display for messages
set updatetime=300              " Faster diagnostic messages

set wildignore=*.pyc,*.cm*,*.o,*.pyc,*.out,*.aux,*.pdf,*.log,*.omc,*.doc
set wildmenu                    " Show list instead of just completing
set wildmode=list:longest,full  " Command <Tab> completion, list matches, then longest common part, then all.

set whichwrap=b,s,h,l,<,>,[,]   " Backspace and cursor keys wrap too
set scrolljump=5                " Lines to scroll when cursor leaves screen
set scrolloff=3                 " Minimum lines to keep above and below cursor
set foldenable                  " Enable folding
set foldlevel=10                " But don't default enabled

set nolist
set listchars-=trail:-          "dont show trailing space
set listchars-=space:-          "dont show space
set listchars-=eol:$            "dont show eol
set listchars+=nbsp:·           "show non breaking space
set listchars+=tab:→\           "show tabs

set termguicolors               " enable true colors support
colorscheme one
let color_mode = $COLOR_MODE

if color_mode == "Dark"
    set background=dark
else
    set background=light
endif
" }}}

" Statusline {{{
set laststatus=2
set report=0

set statusline=%f\                       " Filename
set statusline+=%h%m%r%y                 " Options
set statusline+=%<%=                     " Pad
set statusline+=%{MyCwd()}               " Current dir
set statusline+=\|%{(&fenc==\"\"?&enc:&fenc).((exists(\"+bomb\")\ &&\ &bomb)?\",B\":\"\")}%k            " Encoding
set statusline+=\|c:%-2.c\ b:%-3o\\|%2l:%-L " Right aligned file nav info

hi clear StatusLine
hi StatusLine guibg=LightSteelBlue guifg=White
hi clear StatusLineNC
hi StatusLineNC guibg=LightGray guifg=White ctermbg=19

function! MyCwd()
	return winwidth(0) > 70 ? pathshorten(fnamemodify(getcwd(), ":~")): ''
endfunction
" }}}

" GUI {{{
set guioptions-=T
set guioptions-=m
set icon
set title
" }}}

" Keys and functions {{{
" use jk instead of <Esc>
inoremap jk <Esc>

" edit new file if it doesn't exist
nnoremap gf :e <cfile><CR>

" Insert date and time
:iab <expr> dts strftime("%Y-%m-%d %H:%M")
:iab <expr> ts strftime("%H:%M")

" Save file fast
nnoremap <Leader>w :w<CR>

" Change dir
nnoremap <Leader>cd :cd %:p:h<CR>


map Y y$                                " Yank to end of row

" (de)indent without leaving Visual
vnoremap < <gv
vnoremap > >g

" Sudo save
cmap w!! w !sudo tee % >/dev/null

" Window navigation
nnoremap <C-j> <C-W><C-J>
nnoremap <C-k> <C-W><C-K>
nnoremap <C-l> <C-W><C-L>
nnoremap <C-h> <C-W><C-H>
tnoremap <C-h> <C-\><C-N><C-w>h
tnoremap <C-j> <C-\><C-N><C-w>j
tnoremap <C-k> <C-\><C-N><C-w>k
tnoremap <C-l> <C-\><C-N><C-w>l
inoremap <C-h> <C-\><C-N><C-w>h
inoremap <C-j> <C-\><C-N><C-w>j
inoremap <C-k> <C-\><C-N><C-w>k
inoremap <C-l> <C-\><C-N><C-w>l

" tag
nnoremap <silent> <Leader>ol <C-]>
nnoremap <silent> <Leader>oh <C-O>

" location
nnoremap <silent> <Leader>lc :lclose<CR>zv
nnoremap <silent> <Leader>ll :ll<CR>zv
nnoremap <silent> <Leader>lj :lnext<CR>zv
nnoremap <silent> <Leader>lk :lprev<CR>zv

" quickfix
nnoremap <silent> <Leader>qc :cclose<CR>zv
nnoremap <silent> <Leader>qq :cc<CR>zv
nnoremap <silent> <Leader>qj :cnext<CR>zv
nnoremap <silent> <Leader>qk :cprev<CR>zv

" buffers
nnoremap <silent> <Leader>bl :bnext<CR>
nnoremap <silent> <Leader>bh :bprevious<CR>

" terminal
tnoremap jk <C-\><C-n>

nmap <silent> <C-t>v :vs term://zsh<CR>
nmap <silent> <C-t>s :sp term://zsh<CR>

autocmd BufWinEnter,WinEnter term://* startinsert
autocmd BufLeave term://* stopinsert


" journal files
function! Dag()
	let l:fname = '~/text/dag/' . strftime('%Y-%m-%d') . '.md'
	execute 'edit' l:fname
endfunction
command! Dag call Dag()

" }}}

" Filetypes {{{

" Help files
autocmd BufWritePost ref.txt execute "helptags " . expand("%:p:h")


" Col lenghts
autocmd FileType text setlocal textwidth=78

" ledger rules
autocmd BufRead,BufNewFile rules.sql setlocal noautoindent nocindent nosmartindent indentexpr=

" latex
autocmd FileType tex set textwidth=68

" javascript
autocmd FileType javascript set softtabstop=2 tabstop=2 shiftwidth=2

" markdown & txt
au BufNewFile,BufRead *.txt,*.md setlocal formatoptions=nt1 textwidth=74 wrapmargin=0
let g:vim_markdown_frontmatter = 1

" }}}
